+++
title = "Amy Gao"
description = "This page serves as a quic overview of Amy Gao."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'This page serves as a quic overview of Amy Gao.'
toc = true
top = false
+++

## Quick Start

Amy Gao is currently a first-year Master's Student at Duke University.

