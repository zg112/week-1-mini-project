+++
title = "Amy Gao's Personal Website"


# The homepage contents
[extra]
lead = 'Will gradually build out my person website throughout this semester.'


# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.list]]
title = "Dark mode"
content = "Switch to a low-light UI with the click of a button. Change colors with variables to match your branding."

+++
